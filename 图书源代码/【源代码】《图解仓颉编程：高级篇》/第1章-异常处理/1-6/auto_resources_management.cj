/*
 * 作者：刘玥 张荣超
 * 抖音/微信视频号/微信公众号：九丘教育
 */

public class TestResource <: Resource {
    // closed为true表示关闭状态，为false表示打开状态；初始时资源为打开状态
    private var closed = false

    // 关闭资源
    public func close() {
        if (!closed) {
            closed = true
            println("TestResource：关闭资源成功")
        }
    }

    public func isClosed() {
        println("TestResource的isClosed函数被调用")
        closed
    }

    // 输出TestResource类的信息
    public func printInfo() {
        println("class TestResource <: Resource")
    }

    // 更改closed值
    public func changeClosed(closed: Bool) {
        this.closed = closed
    }

    // 其他成员略
}

// TestResource的包装类
public class TestResourceWrapper <: Resource {
    private var closed = false  // closed为true表示关闭状态，为false表示打开状态
    private var resource: TestResource

    public init(resource: TestResource) {
        this.resource = resource
    }

    public func close() {
        if (!closed) {
            closed = true
            println("TestResourceWrapper：关闭资源成功")
        }

        // 关闭当前TestResourceWrapper实例的同时关闭被包装的TestResource实例
        resource.close()
        println("从TestResourceWrapper中关闭TestResource成功")
    }

    public func isClosed() {
        closed
    }

    // 输出TestResourceWrapper包装的TestResource实例的信息
    public func printInfo() {
        resource.printInfo()
    }

    // 其他成员略
}

main() {
    try (
        // 如果资源间存在依赖关系，要先实例化被依赖的资源
        resource = TestResource(),
        resourceWrapper = TestResourceWrapper(resource)
    ) {
        resourceWrapper.printInfo()
    }
}
